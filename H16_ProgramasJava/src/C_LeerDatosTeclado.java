import java.util.Scanner;

public class C_LeerDatosTeclado {

    public static void main(String[] args) {

        // Declarar la variable para leer datos de teclado con la Clase Scanner
        Scanner leerDatosTeclado = new Scanner(System.in);

        System.out.println("Digite su edad: ");
        String edad1 = leerDatosTeclado.next();
        System.out.println("edad tipo string = " + edad1);

        System.out.println("Digite un valor entero");
        int valor = leerDatosTeclado.nextInt();

        int mult = valor * 6;
        System.out.println("valor * 6 = " + mult);
    }

}
