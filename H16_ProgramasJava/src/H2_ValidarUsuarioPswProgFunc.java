import java.util.Scanner;

public class H2_ValidarUsuarioPswProgFunc {

    // Programa para validar Usuario y Contraseña en una cantidad de intentos..

    public static void main(String[] args) {

        boolean banderaUserPswCheck = false;

        // Entrada - Transformación
        banderaUserPswCheck = validarUserPsw();


        // Salida
        // if (banderaUserPswCheck == true){
        if (banderaUserPswCheck){
            // Ingresa al Programa...
            // Llamado a la función del Programa...
            programa();
        }
        else {
            // Agotó los intentos sin validar usuario y contraseña...
            System.out.println("\nHa agotado el número de intentos...\nEspere 1 hora para volver a ingresar...\n");
        }

        System.out.println("Fin del Programa...");

    }

    public static boolean validarUserPsw (){
        // Simulación de la obtención de una base de datos
        String userGuardada = "tripulantes", pswGuardada = "Abc123";

        // Declarar Variables
        String user, psw;
        Scanner input = new Scanner(System.in);
        byte numInt = 6, cont0 = 0;
        boolean banderaUserPswCheck = false;


        do {
            if (cont0 != 0){
                System.out.println("Intente nuevamente...");
                System.out.println("Le quedan " + (numInt - cont0) + " intentos...\n");
            }
            cont0++;
            // Entrandas
            System.out.print("Digite su Usuario: ");
            user = input.next();
            System.out.print("Digite su Contraseña: ");
            psw = input.next();

            // Transformación
            if (user.equals(userGuardada) && psw.equals(pswGuardada)){
                System.out.println("\nUsuario y Contraseña Validados...\n");
                banderaUserPswCheck = true;
            }
            else {
                System.out.println("\nUsuario y/o Contraseña Incorrectos...");
                /*System.out.println("Intente nuevamente...");
                System.out.println("Le quedan " + (numInt - cont0) + " intentos...\n");*/
            }
        } while (cont0 < numInt && !banderaUserPswCheck);
        // while (cont <= numInt && banderaUserPswCheck == false);

        return banderaUserPswCheck;
    }

    public static void programa(){
        System.out.println("Bienvenido al SoftMinTIC UIS... Desarrolladores de Software Profesional");
        // Declarar Variables
        Scanner leerDatos = new Scanner(System.in);
        String opcion;
        int cont;

        // Ejecución del Programa

        do {
            System.out.println("####### Menú de Opciones ######");
            System.out.println("1. Ejecutar while");
            System.out.println("2. Ejecutar do-while");
            System.out.println("3. Ejecutar for");
            System.out.println("4. Operador Ternario");
            System.out.println("0. Terminar o Salir");
            System.out.println("Digite un número del menú anterior: ");
            opcion = leerDatos.next();

            switch (opcion){
                case "1":
                    //int cont = 0;
                    cont = 0;
                    while (cont < 6){
                        System.out.println("cont = " + cont);
                        cont++;
                    }
                    System.out.println("cont finaliza en: " + cont);
                    break;

                case "2":
                    cont = 0;
                    do {
                        System.out.println("cont = " + cont);
                        cont += 3;
                    } while (cont < 10);
                    System.out.println("cont finaliza en: " + cont);
                    break;

                case "3":
                    //for (int i = 0; i < 3; i++) {
                    int i; // la declaro
                    for (i = 0; i < 3; i++) {
                        System.out.println("i = " + i);
                    }
                    System.out.println("i finaliza en: " + i);
                    break;

                case "4":
                    System.out.println("Digite \"S\" o \"N\"");
                    String texto = leerDatos.next();
                    System.out.println("texto = " + texto);

                    texto = (texto.equals("S"))? "Seleccionó S": "Seleccionó N";
                    System.out.println("texto = " + texto);
                    break;

                case "0":
                    System.out.println("Realmente desea finalizar el Programa (S/N)?");
                    opcion = leerDatos.next();

                    if (opcion.equals("S") || opcion.equals("s")){
                        opcion = "0";
                        System.out.println("Gracias por utilizar SoftMinTIC UIS...!");
                        System.out.println("Desarrolladores de Software Profesional...!");
                    }
                    else { // redundancia de código
                        opcion = "";
                    }
                    break;

                default:
                    System.out.println("Por favor seleccione una opción válida del menú");
                    break;

            }
        } while (!opcion.equals("0") && opcion != null);
    }
}
