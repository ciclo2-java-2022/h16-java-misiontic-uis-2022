public class K_Main_EjemplSimilarReto2 {

    /*La Empresa X, distribuye producto lácteos, entre ellos
     * Leche deslatosada y Leche entera en empaques de Litro.
     * Si la leche es deslatosada el precio es de 4000
     * Si la leche no es deslatosada el precio es de 3000.
     * Llegado el caso que un minimercado realice la cotización,
     * Se debe generar la siguiente información:
     * Nombre, Código, Categoría, Descripción (Entera/Deslatosada), Empresa,
     * Precio (el cual debe tenerse en cuenta si es deslatosada o no).
     * Tener en cuenta el diagrama de clases Diseñado Previamente.*/

    public static void main(String[] args) {

        System.out.println("---------------------------------");

        // Declarar los Objetos de la Clase ProductoLeche
        K_ProductoLeche_EjemplSimilarReto2 leche1, leche2;

        // Instanciar el Objeto leche1
        leche1 = new K_ProductoLeche_EjemplSimilarReto2("Mi Vaquita", "123asd456", "Lácteo", "Leche Deslactosada", "Freskaleche", true);
        leche1.info();

        System.out.println("---------------------------------");
        // Instanciar el Objeto leche2
        leche2 = new K_ProductoLeche_EjemplSimilarReto2("Vaquísima", "12as34df56", "Lácteo", "Leche Entera", "Freskaleche", false);
        leche2.info();
        System.out.println("---------------------------------");

        // Declarar e Instanciar el Objeto leche3
        K_ProductoLeche_EjemplSimilarReto2 leche3 = new K_ProductoLeche_EjemplSimilarReto2("Varica", "654321", "Lácteo", "Leche Deslatosada", "Freskaleche", true);
        leche3.info();
        System.out.println("---------------------------------");
    }

}
