package m_agregacion_vs_composicion;

public class Main {
    public static void main(String[] args) {

        System.out.println("------------------------------------");
        Celular cel1 = new Celular();
        cel1.mostrarInformacion();

        // Continuar => Crear celular con información y agregar simCards...
        // Ejecutar Secuencia paso a paso... debug...

        System.out.println("------------------------------------");
        System.out.println("Crear celular");
        Celular celular = new Celular("Apple", "iPhone 6", 1234567890123L, "Bat123rf456", 5000f, (byte) 1);
        celular.mostrarInformacion();

        System.out.println("------------------------------------");
        System.out.println("Crear SimCard");
        SimCard sim = new SimCard("Wom", 3233214566l);
        sim.informacionSimCard();

        SimCard sim2 = new SimCard("Claro", 3212218766l);
        sim2.informacionSimCard();

        System.out.println("------------------------------------");

        System.out.println("Agregar SimCard al Celular");
        celular.agregarSimCard(sim);
        celular.mostrarInformacion();

        System.out.println("------------------------------------");

        System.out.println("------------------------------------");
        System.out.println("Crear celular 2");
        Celular celular2 = new Celular("Motorola", "MotoG", 3214567890123L, "Bat321rf654", 4000f, (byte) 2);
        celular2.mostrarInformacion();

        System.out.println("------------------------------------");

        System.out.println("Agregar SimCard al Celular 2");
        celular2.agregarSimCard(sim);
        celular2.agregarSimCard(sim2);
        celular2.mostrarInformacion();

        System.out.println("------------------------------------");

        System.out.println("Agregar Otra SimCard al Celular 1");
        celular.agregarSimCard(sim);
        celular.mostrarInformacion();

        System.out.println("------------------------------------");


    }
}
