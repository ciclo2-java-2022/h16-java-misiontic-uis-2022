package m_agregacion_vs_composicion;

public class Bateria {
    // Atributos
    private String refBat;
    private float ampBat;

    // Constructores

    public Bateria() {}

    public Bateria(String refBat, float ampBat) {
        this.refBat = refBat;
        this.ampBat = ampBat;
    }

    // Métodos - Funciones
    public void informacionBateria(){
        System.out.println("Referencia Batería: " + this.refBat);
        System.out.println("Capacidad Batería (mA): " + this.ampBat);
    }
}
