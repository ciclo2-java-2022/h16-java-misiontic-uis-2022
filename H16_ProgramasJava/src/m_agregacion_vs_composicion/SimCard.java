package m_agregacion_vs_composicion;

public class SimCard {
    // Atributos
    private String empresa;
    private long numero;

    // Contructores
    public SimCard() {}

    public SimCard(String empresa, long numero) {
        this.empresa = empresa;
        this.numero = numero;
    }

    // Métodos - Funciones
    public void informacionSimCard(){
        System.out.println("Empresa Operadora: " + this.empresa);
        System.out.println("Número: " + this.numero);
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }
}
