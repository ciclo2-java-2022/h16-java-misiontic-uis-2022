package r_reto4_o_reto3_ejersimilar.o_reto3_ejersimilar;

public class O_Main {

    public static void main(String[] args) {

        ProductoRefrigerado prod1 = new ProductoRefrigerado();
        prod1.setPrecioBase(4000.0f);
        System.out.println("Precio Almacenamieto ProdRefrig: " + prod1.calcularPrecioAlmacenamiento());

        ProductoNoRefrigerado prod2 = new ProductoNoRefrigerado();
        prod2.setPrecioBase(2000.0f);
        System.out.println("Precio Almacenamieto Prod_No_Refrig: " + prod2.calcularPrecioAlmacenamiento());
    }
}
