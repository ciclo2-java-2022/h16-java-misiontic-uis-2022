package r_reto4_o_reto3_ejersimilar.o_reto3_ejersimilar;

public class ProductoRefrigerado extends Producto {

    // Atributos

    // Constructor

    public ProductoRefrigerado() {}

    public ProductoRefrigerado(String nombre, String id, float precioBase, float tempAlmac) {
        super(nombre, id, precioBase, tempAlmac);
    }

    @Override
    public float calcularPrecioAlmacenamiento() {
        return getPrecioBase() * 1.3f;
    }
}
