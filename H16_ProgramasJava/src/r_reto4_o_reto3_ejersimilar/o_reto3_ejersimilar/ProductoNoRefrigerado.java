package r_reto4_o_reto3_ejersimilar.o_reto3_ejersimilar;

public class ProductoNoRefrigerado extends Producto{
    // Atributos

    // Constructor

    public ProductoNoRefrigerado() {}

    public ProductoNoRefrigerado(String nombre, String id, float precioBase, float tempAlmac) {
        super(nombre, id, precioBase, tempAlmac);
    }

    @Override
    public float calcularPrecioAlmacenamiento() {
        float precioAlmac;
        // precioAlmac = getPrecioBase() + getPrecioBase() * 15 / 100;
        // precioAlmac = getPrecioBase() * (1 + 0.15);
        precioAlmac = getPrecioBase() * 1.15f;
        return precioAlmac;
    }
}
