package r_reto4_o_reto3_ejersimilar.o_reto3_ejersimilar;

public abstract class Producto {
    // Atributos
    private String nombre, id;
    private float precioBase, tempAlmac;

    // Constructor

    public Producto() {}

    public Producto(String nombre, String id, float precioBase, float tempAlmac) {
        this.nombre = nombre;
        this.id = id;
        this.precioBase = precioBase;
        this.tempAlmac = tempAlmac;
    }

    // Métodos - Funciones
    public abstract float calcularPrecioAlmacenamiento();

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getPrecioBase() {
        return precioBase;
    }

    public void setPrecioBase(float precioBase) {
        this.precioBase = precioBase;
    }

    public float getTempAlmac() {
        return tempAlmac;
    }

    public void setTempAlmac(float tempAlmac) {
        this.tempAlmac = tempAlmac;
    }
}
