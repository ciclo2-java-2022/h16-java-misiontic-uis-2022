package s_collections;

public class S1_Main_Array {

    public static void main(String[] args) {
        System.out.println("------------------------------------------");
        // Creación de Objetos
        String nombre = "Hugo";
        int codigo = 123;
        // Declarar el objeto
        S_Tripulantes trip1;
        // Instanciar el objeto
        trip1 = new S_Tripulantes();
        trip1.mostrarInformacion();
        trip1.setNombre(nombre);
        trip1.setCodigo(codigo);
        trip1.mostrarInformacion();
        System.out.println("Nombre método get: " + trip1.getNombre());
        System.out.println("Código método get: " + trip1.getCodigo());

        System.out.println("------------------------------------------");
        String nombre2 = "Paco";
        int codigo2 = 321;
        S_Tripulantes trip2 = new S_Tripulantes(nombre2, codigo2);
        trip2.mostrarInformacion();

        System.out.println("------------------------------------------");
        S_Tripulantes trip3 = new S_Tripulantes("Luis", 312);
        trip3.mostrarInformacion();

        System.out.println("------------------------------------------");

        // Asignar los Objetos a un arreglo de Objetos
        // Declarar e instanciar el arreglo de objetos
        //S_Tripulantes [] tripulantes = new S_Tripulantes[10];
        S_Tripulantes tripulantes[] = new S_Tripulantes[10];
        tripulantes[0] = trip1;
        tripulantes[1] = trip2;
        tripulantes[2] = trip3;
        tripulantes[3] = new S_Tripulantes("JAS", 666);

        System.out.println("Uso de fori => Únicamente recorriendo los elementos de la lista");
        for (int i = 0; i < tripulantes.length; i++) {
            System.out.println("tripulantes[" + i + "]: " + tripulantes[i]);
        }
        System.out.println("------------------------------------------");
        System.out.println("Uso de fori => Únicamente recorriendo los elementos de la lista y mostrando la información");
        for (int i = 0; i < tripulantes.length; i++) {
            System.out.println("tripulantes[" + i + "]: " );
            if (tripulantes[i] != null){
                tripulantes[i].mostrarInformacion();
            }
            else {
                System.out.println("null");
            }
        }
        System.out.println("------------------------------------------");
        System.out.println("Uso de foreach => Únicamente recorriendo los elementos de la lista");
        for (S_Tripulantes i : tripulantes) {
            System.out.println("valor q toma i: " + i);
        }
        System.out.println("------------------------------------------");
        System.out.println("Uso de foreach => Únicamente recorriendo los elementos de la lista y mostrando la información");
        for (S_Tripulantes i : tripulantes) {
            System.out.println("valor que toma i: ");
            if (i == null){
                System.out.println("null");
            }
            else {
                i.mostrarInformacion();
            }
        }
        System.out.println("------------------------------------------");
    }
}
