package s_collections;

import java.util.Iterator;
import java.util.ArrayList;

public class S2_Main_List_ArrayList {

    public static void main(String[] args) {
        // Creación de Objetos
        String nombre = "Hugo";
        int codigo = 123;
        // Declarar el objeto
        S_Tripulantes trip1;
        // Instanciar el objeto
        trip1 = new S_Tripulantes();
        trip1.mostrarInformacion();
        trip1.setNombre(nombre);
        trip1.setCodigo(codigo);
        trip1.mostrarInformacion();
        System.out.println("Nombre método get: " + trip1.getNombre());
        System.out.println("Código método get: " + trip1.getCodigo());

        System.out.println("------------------------------------------");
        String nombre2 = "Paco";
        int codigo2 = 321;
        S_Tripulantes trip2 = new S_Tripulantes(nombre2, codigo2);
        trip2.mostrarInformacion();

        System.out.println("------------------------------------------");
        S_Tripulantes trip3 = new S_Tripulantes("Luis", 312);
        trip3.mostrarInformacion();

        System.out.println("------------------------------------------");

        // Asignar los Objetos a un arreglo de Objetos - ArryList
        // Declarar e Instanciar el Array List
        ArrayList<S_Tripulantes> tripulantes = new ArrayList<S_Tripulantes>();
        //List<S_Tripulantes> tripulantes = new ArrayList<S_Tripulantes>();
        tripulantes.add(trip1);
        tripulantes.add(trip2);
        tripulantes.add(trip3);
        tripulantes.add(new S_Tripulantes("JAS", 666));

        System.out.println("Uso de fori => Únicamente recorriendo los elementos de la lista");
        for (int i = 0; i < tripulantes.size(); i++) {
            System.out.println("tripulantes[" + i + "]: " + tripulantes.get(i));
        }
        System.out.println("------------------------------------------");
        System.out.println("Uso de fori => Únicamente recorriendo los elementos de la lista y mostrando la información");
        for (int i = 0; i < tripulantes.size(); i++) {
            System.out.println("tripulantes[" + i + "]: ");
            tripulantes.get(i).mostrarInformacion();
        }
        System.out.println("------------------------------------------");
        System.out.println("Uso de foreach => Únicamente recorriendo los elementos de la lista");
        for (S_Tripulantes i : tripulantes) {
            System.out.println("valor que toma i: " + i);
        }

        System.out.println("------------------------------------------");
        System.out.println("Uso de foreach => Únicamente recorriendo los elementos de la lista y mostrando la información");
        for (S_Tripulantes i : tripulantes) {
            System.out.println("valor que toma i: " + i);
            i.mostrarInformacion();
        }

        System.out.println("------------------------------------------");
        System.out.println("Recorrer la Lista con Iterator");
        Iterator<S_Tripulantes> i = tripulantes.iterator();
        while (i.hasNext()){
            /*i.next().mostrarInformación();
            System.out.println("Nombre: " + i.next().getNombre());
            System.out.println("Código: " + i.next().getCodigo());*/
            S_Tripulantes copiaDei = i.next();
            System.out.println("valor de i.next(): " + copiaDei);
            copiaDei.mostrarInformacion();
            System.out.println("Nombre método get: " + copiaDei.getNombre());
            System.out.println("Código método get: " + copiaDei.getCodigo());
        }

        System.out.println("------------------------------------------");
    }
}
