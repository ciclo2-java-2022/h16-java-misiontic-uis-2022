import java.util.Scanner;

public class I_CalculadoraConFunciones {

    public static void main(String[] args) {

        // Programa Calculadora con Funciones

        // Declarar Variables
        Scanner leerDatos = new Scanner(System.in);
        String opcion;
        int cont;
        float resultado;

        float dolares = D2_convPesosDolaresFuncion.convPesosDolares(1000000f);
        System.out.println("Dolares: " + dolares);


        // Ejecución del Programa

        do {
            System.out.println("####### Menú de Opciones ######");
            System.out.println("1. Sumar 2 Números");
            System.out.println("2. Restar 2 Números");
            System.out.println("3. Multiplicar 2 Números");
            System.out.println("4. Dividir 2 Números");
            System.out.println("0. Terminar o Salir");
            System.out.println("Digite un número del menú anterior: ");
            opcion = leerDatos.next();

            switch (opcion){
                case "1":
                    resultado = suma2Num(solValor((byte) 1), solValor((byte) 2));
                    System.out.println("El Resultado es: " + resultado);
                    leerDatos.next();
                    break;

                case "2":
                    resultado = resta2Num(solValor((byte) 1), solValor((byte) 2));
                    System.out.println("El Resultado es: " + resultado);
                    leerDatos.next();
                    break;

                case "3":
                    resultado = multiplicacion2Num(solValor((byte) 1), solValor((byte) 2));
                    System.out.println("El Resultado es: " + resultado);
                    leerDatos.next();
                    break;

                case "4":
                    resultado = division2Num(solValor((byte) 1), solValor((byte) 2));
                    System.out.println("El Resultado es: " + resultado);
                    leerDatos.next();
                    break;

                case "0":
                    System.out.println("Realmente desea finalizar el Programa (S/N)?");
                    opcion = leerDatos.next();

                    if (opcion.equals("S") || opcion.equals("s")){
                        opcion = "0";
                        System.out.println("Gracias por utilizar SoftMinTIC UIS...!");
                        System.out.println("Desarrolladores de Software Profesional...!");
                    }
                    else { // redundancia de código
                        opcion = "";
                    }
                    break;

                default:
                    System.out.println("Por favor seleccione una opción válida del menú");
                    break;

            }
        } while (!opcion.equals("0") && opcion != null);

        System.out.println("Fin del Programa...");
    }

    public static float solValor(byte val){
        Scanner input = new Scanner(System.in);
        System.out.print("Digite el valor número " + val + ": ");
        float num = input.nextFloat();
        return num;
    }

    public static float suma2Num(float num1, float num2){
        // Antradas en los parámetros o argumentos
        // Transforamción => la operación matemática
        float resultado = num1 + num2;
        // Salida
        return resultado;
    }

    public static float resta2Num(float num1, float num2){
        return num1 - num2;
    }

    public static float multiplicacion2Num(float num1, float num2){
        return num1 * num2;
    }

    public static float division2Num(float num1, float num2){
        return num1 / num2;
    }
}
