import java.util.Scanner;

public class G_VectoresNotas {

    public static void main(String[] args) {
        // Programa para ingresar notas de 5 cortes y calcular el promedio o nota definitiva

        // Declarar Variables
        float notas[] = new float[5], notaDef = 0.0f;
        Scanner leerDatos = new Scanner(System.in);
        byte cantNotas = (byte) notas.length;

        // Ejecución del Programa

        /*for (int i = 0; i < 5; i++) {
            System.out.println("notas[" + i + "] = " + notas[i]);
        }*/
        // Entradas
        System.out.println("Digite las notas de los 5 retos");
        for (int i = 0; i < notas.length; i++) {
            System.out.println("Digite el valor de la nota " + (i + 1));
            notas[i] = leerDatos.nextFloat();
            /*String datos = leerDatos.next();
            notas[i] = (float) datos;*/
        }

        // Transformación => hallar el promedio
        for (int i = 0; i < cantNotas; i++) {
            // notaDef = notaDef + notas[i];
            notaDef += notas[i];
        }
        // notaDef = notaDef / cantNotas;
        notaDef /= cantNotas;
        notaDef = Math.round(notaDef * 100.0f) / 100.0f;

        // Salida
        System.out.println("Las Notas Digitadas son:");
        for (int i = 0; i < 5; i++) {
            //System.out.println("notas[" + (i) + "] = " + notas[i]);
            System.out.println("notas[" + (i + 1) + "] = " + notas[i]);
        }
        System.out.println("Promedio = " + notaDef);

        int vector[] = {1, 2, 3, 4, 5};
        for (int i = 0; i < vector.length; i++) {
            System.out.println("vector[" + i + "] = " + vector[i]);
        }
    }
}
