package t_enumerations;

public class T_Main_Enum {
    enum Continente {
        AFRICA(54),
        EUROPA(50),
        ASIA(49),
        AMERICA(35),
        OCEANIA(15);
        // Atributos
        private int numeroPaises;
        // Constructor
        Continente(){}

        Continente(int numeroPaises) {
            this.numeroPaises = numeroPaises;
        }

        // Métodos - Funciones

        public int getNumeroPaises() {
            return numeroPaises;
        }

        public void setNumeroPaises(int numeroPaises) {
            this.numeroPaises = numeroPaises;
        }


    }

    public static void main(String[] args) {

        Continente miCont = Continente.AMERICA;
        System.out.print("\nmiCont: " + miCont);
        System.out.print(" tiene: " + miCont.getNumeroPaises() + " paises. \n");

        Continente otro = Continente.EUROPA;
        System.out.print("\nmiCont: " + otro);
        System.out.print(" tiene: " + otro.getNumeroPaises() + " paises. \n");

        System.out.print("\nLos continentes " + miCont + " y " + otro +
                " suman " + (miCont.getNumeroPaises() + otro.getNumeroPaises()) + " paises...\n");

        final double PI = 3.1416; // final para valores constantes o inmutables.
        //PI = 3.0; // genera error...


    }
}
