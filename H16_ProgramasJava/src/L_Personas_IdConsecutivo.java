public class L_Personas_IdConsecutivo {
    // Atributos
    private static long consecutivo = 1198606110L; // 9999999999
    private long id;
    private String nombre, apellido;

    // Constructo
    public L_Personas_IdConsecutivo() {
        consecutivo++;
        this.id = consecutivo;
        //consecutivo++;
    }

    public L_Personas_IdConsecutivo(String nombre, String apellido) {
        consecutivo++;
        this.id = consecutivo;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    // Métodos - Funciones

    public long getId() {
        return id;
    }

    /*public void setId(long id) {
        this.id = id;
    }*/

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Override
    public String toString() {
        return "L_Personas_IdConsecutivo{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                '}';
    }
}
