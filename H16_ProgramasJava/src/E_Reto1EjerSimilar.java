public class E_Reto1EjerSimilar {

    public static void main(String[] args) {
        /*La Fábrica X, requiere determinar el costo de un producto,
        * incluyendo el tipo de empaque que se requiere:
        * Tener en cuenta un valor base de producción.
        * Si el producto requiere empaque al vacío, el costo incrementa un 40%
        * De lo contrario, el costo incrementa un 20%*/

        // Declaración de Variables
        float valorBase, precioTotal;
        boolean empaqueVacio;

        // Entrada(s)
        valorBase = 10000;
        empaqueVacio = false;

        // Transformación - Llamado a la función
        precioTotal = calcularPrecioTotal(empaqueVacio, valorBase);

        // Salida(s)
        System.out.println("Precio Total: " + precioTotal);

    }

    public static float calcularPrecioTotal(boolean empaqueVacio, float precioBase){
        // Declarar Variables
        float precioTotal;
        // if (empaqueVacio == true){
        if (empaqueVacio){
            // precioTotal = precioBase + precioBase * 40 / 100;
            // precioTotal = precioBase * (1 + 0.4);
            precioTotal = precioBase * 1.4f;
        }
        else {
            precioTotal = precioBase * 1.2f;
        }
        // Salida
        return precioTotal;
    }
}
