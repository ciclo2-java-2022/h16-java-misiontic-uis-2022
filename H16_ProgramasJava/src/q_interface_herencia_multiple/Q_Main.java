package q_interface_herencia_multiple;

public class Q_Main {
    public static void main(String[] args) {
        System.out.println("---------------------------");
        Espagnol idiomEsp = new Espagnol();
        idiomEsp.decirHola();
        idiomEsp.buenosDias();
        idiomEsp.despedida();
        System.out.println("---------------------------");
        Ingles ingles = new Ingles();
        ingles.buenosDias();
        ingles.decirHola();
        ingles.despedida();
        System.out.println("---------------------------");
        Frances frances = new Frances();
        frances.decirHola();
        frances.buenosDias();
        frances.despedida();
        System.out.println("---------------------------");

    }
}
