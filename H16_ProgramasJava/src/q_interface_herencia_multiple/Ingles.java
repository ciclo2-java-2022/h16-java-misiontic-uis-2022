package q_interface_herencia_multiple;

public class Ingles extends LenguajeComunicacion implements Idioma{
    // Atributos
    // Constructor

    // Métodos - Funciones

    @Override
    public void decirHola() {
        System.out.println("Hi...");
    }

    @Override
    public void buenosDias() {
        System.out.println("Good morning...");
    }
}
