package q_interface_herencia_multiple;

public interface Idioma {

    // Las interfaces NO tienen atributos ni constructor....

    // Métodos - Funciones
    // Por defecto todos los métodos son de tipo abstracto
    // public abstract void decirHola();
    void decirHola();
}
