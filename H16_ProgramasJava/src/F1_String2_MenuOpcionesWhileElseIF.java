import java.util.Scanner;

public class F1_String2_MenuOpcionesWhileElseIF {

    public static void main(String[] args) {
        // Declarar Variables
        Scanner leerDatos = new Scanner(System.in);
        String opcion = "", op;

        while (!opcion.equals("0")){
            System.out.println("####### Menú de Opciones ######");
            System.out.println("1. Primer caso...");
            System.out.println("2. Segundo caso...");
            System.out.println("3. Tercero caso...");
            System.out.println("4. Cuarto caso...");
            System.out.println("0. Terminar o Salir");
            System.out.println("Digite un número del menú anterior: ");
            opcion = leerDatos.next();

            if(opcion.equals("1")){
                System.out.println("Seleccionó la opción 1...");
                System.out.println("Digite enter para continuar...");
                op = leerDatos.next();
            }
            else if (opcion.equals("2")){
                System.out.println("Seleccionó la opción 2...");
                System.out.println("Digite enter para continuar...");
                op = leerDatos.next();
            }
            else if (opcion.equals("3")){
                System.out.println("Seleccionó la opción 3...");
                System.out.println("Digite enter para continuar...");
                op = leerDatos.next();
            }
            else if (opcion.equals("4")){
                System.out.println("Seleccionó la opción 4...");
                System.out.println("Digite enter para continuar...");
                op = leerDatos.next();
            }
            else if (opcion.equals("0")){
                System.out.println("Seleccionó la opción 0...");
                System.out.println("Digite enter para continuar...");
                op = leerDatos.next();
            }
            else {
                System.out.println("Por favor seleccione una opción válida del menú...");
                System.out.println("Digite enter para continuar...");
                op = leerDatos.next();
            }
        }
    }
}
