package w_conversor_divisas;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class W_ConversorDivisas extends JFrame{
    // Atributos
    private JPanel panel1;
    private JPanel panel2;
    private JTextField textField1PD;
    private JTextField textField3DP;
    private JTextField textField2PD;
    private JTextField textField4DP;
    private JButton pesosDolaresButton;
    private JButton dolaresPesosButton;
    private JButton borrarTodoButton;
    private JPanel panel;
    private static double $pesos1Dolar = 4395.63; // pesos colombianos

    // Constructor
    public W_ConversorDivisas(){
        setContentPane(panel);
        pack();
        pesosDolaresButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Código primer botón
                // Declarar variables
                double dolares, pesos;
                // Entrada
                pesos = Double.parseDouble(textField1PD.getText());
                // Transformación
                dolares = Math.round((pesos * 1 / $pesos1Dolar) * 100.0) / 100.0;
                // Salida
                textField2PD.setText(String.valueOf(dolares));

                textField3DP.setText("1");
                textField4DP.setText(String.valueOf($pesos1Dolar));
            }
        });
        dolaresPesosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Código segundo botón
                // Declarar variables
                double dolares, pesos;
                // Entrada
                dolares = Double.parseDouble(textField3DP.getText());
                // Transformación
                pesos = Math.round((dolares * $pesos1Dolar / 1.0) * 100.0) / 100.0;
                // Salida
                textField4DP.setText(String.valueOf(pesos));

                textField1PD.setText(String.valueOf($pesos1Dolar));
                textField2PD.setText("1");
            }
        });
        borrarTodoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textField1PD.setText("");
                textField2PD.setText("");
                textField3DP.setText("");
                textField4DP.setText("");
            }
        });
    }

    // Métodos - Funciones
    public static void main(String[] args) {
        W_ConversorDivisas form = new W_ConversorDivisas();
        form.setSize(800, 600);
        form.setVisible(true);
        form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
