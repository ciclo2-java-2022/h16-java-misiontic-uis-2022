package v_gui_swing;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class V2_GUI_Swing_Clase_Codigo extends JFrame{
    // Atributos
    private JLabel label1, label2, label3, label4;
    private JPanel panel1, panel2;
    private JTextField cajaTexto1, cajaTexto2;
    private JButton button;

    // Constructor
    public V2_GUI_Swing_Clase_Codigo(){
        panel1 = new JPanel();
        panel1.setBounds(10, 10, 300, 200);
        label1 = new JLabel("Hola Tripulantes");
        label1.setBounds(20, 50, 200, 15);

        label2 = new JLabel("SoftMinTIC UIS ver. 0.0.1");
        label2.setBounds(20, 150, 150, 20);
        panel1.add(label1, label2);
        add(panel1);

        panel2 = new JPanel();
        panel2.setBounds(400, 10, 300, 200);
        label3 = new JLabel("Información adicional...");
        label3.setBounds(20, 60, 200, 25);
        panel2.add(label3);
        add(panel2);

        this.label4 = new JLabel("Caja de Texto:");
        label4.setBounds(30, 300, 200, 20);
        add(label4);

        cajaTexto1 = new JTextField();
        cajaTexto1.setBounds(30, 360, 300, 20);
        add(cajaTexto1);

        cajaTexto2 = new JTextField();
        cajaTexto2.setBounds(30, 400, 300, 20);
        cajaTexto2.setText("Caja de Texto 2.");
        add(cajaTexto2);

        button = new JButton("Click Aquí");
        button.setBounds(50, 500, 100, 30);
        panel2.add(button);
        button.addActionListener(this::actionPerfomed);

        /*button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Mensaje oculto para programadores...");
                String x = cajaTexto1.getText();
                cajaTexto1.setText("Magia Tripulantes...!");
                cajaTexto2.setText("Cambió a: " + x);
            }
        });*/


    }

    // Métodos - Funciones
    public void actionPerfomed(ActionEvent e){
        System.out.println("Mensaje oculto para programadores...");
        String x = cajaTexto1.getText();
        cajaTexto1.setText("Magia Tripulantes...!");
        cajaTexto2.setText("Cambió a: " + x);
    }
}
