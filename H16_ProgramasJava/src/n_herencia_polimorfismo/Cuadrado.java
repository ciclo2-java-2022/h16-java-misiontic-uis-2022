package n_herencia_polimorfismo;

public class Cuadrado extends FigurasGeometricas {

    // Atributos
    private double lado;

    // Constructores

    public Cuadrado() {}

    public Cuadrado(String nombre, byte cantLados, double lado) {
        super(nombre, cantLados);
        this.lado = lado;
    }

    // Métodos - Funciones

    @Override
    public double areaFigura() {
        //double area = this.lado * this.lado;
        double area = Math.pow(this.lado, 2);
        //return super.areaFigura();
        return Math.round(area * 100.0) / 100.0;
    }

    public double getLado() {
        return lado;
    }

    public void setLado(double lado) {
        this.lado = lado;
    }
}
