package n_herencia_polimorfismo;

public abstract class FigurasGeometricas {

    // Atributos
    private String nombre;
    private byte cantLados;

    // Constructor

    public FigurasGeometricas() {}

    public FigurasGeometricas(String nombre, byte cantLados) {
        this.nombre = nombre;
        this.cantLados = cantLados;
    }

    // Métodos - Funciones

    //public double areaFigura(){return 0.0;}
    // Una función abstracta únicamente puede estar contenida dentro de una clase abstracta...
    public abstract double areaFigura();

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public byte getCantLados() {
        return cantLados;
    }

    public void setCantLados(byte cantLados) {
        this.cantLados = cantLados;
    }
}
