package n_herencia_polimorfismo;

public class Rectangulo extends FigurasGeometricas {

    // Atributos
    private double base, altura;

    // Constructores

    public Rectangulo() {}

    public Rectangulo(String nombre, byte cantLados, double base, double altura) {
        super(nombre, cantLados);
        this.base = base;
        this.altura = altura;
    }

    // Métodos - Funciones

    @Override
    public double areaFigura() {
        //return super.areaFigura();
        return this.base * this.altura;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }
}
