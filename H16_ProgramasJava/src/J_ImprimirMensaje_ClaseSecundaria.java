public class J_ImprimirMensaje_ClaseSecundaria {

    // Atributos - Características
    private String mensaje;

    // Métodos o Funciones - Funcionalidad

    public J_ImprimirMensaje_ClaseSecundaria(){}

    public J_ImprimirMensaje_ClaseSecundaria(String mensaje){
        this.mensaje = mensaje;
    }

    public static void funMensaje1(){
        String msj = "Mensaje de la Función 1";
        System.out.println(msj);
    }

    public boolean funMensaje2(){
        boolean bool = true;
        System.out.println("El Atributo Contiene: " + this.mensaje);
        return bool;
    }
}
