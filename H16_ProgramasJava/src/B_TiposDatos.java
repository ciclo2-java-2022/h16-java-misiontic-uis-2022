public class B_TiposDatos {

    public static void main(String[] args) {
        // Declaración de Variables
        byte a;

        // Asignación del valor de la variable
        a = 127;

        System.out.println("a = " + a);
        /*a = a + 1;*/
        System.out.println("a + 1 = " + (a + 1));

        int b;
        b = a + 1;
        System.out.println("b = " + b);

        float c = 100000000000000000000000000000000f;
        System.out.println("c = " + c);
        c = c + 1;

        System.out.println("c + 1 = " + c);

        System.out.print("El de la variable ");
        System.out.print("a = " + a);

        System.out.println("Digite su edad: ");
        // Lectura con System.console().readLine(); => únicamente para utilizar desde la terminal o el cmd
        /*String edad = System.console().readLine();

        System.out.println("edad = " + edad);*/

    }
}
