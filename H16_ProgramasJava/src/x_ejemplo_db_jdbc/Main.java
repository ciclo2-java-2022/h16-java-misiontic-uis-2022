package x_ejemplo_db_jdbc;

import x_ejemplo_db_jdbc.controlador_persistencia.ConexionBD;

import javax.swing.*;
import java.sql.Connection;
import java.sql.ResultSet;

public class Main {

    public static void main(String[] args) {

        ConexionBD obj = new ConexionBD();

        try (Connection cnx = obj.getConnection()) {
            System.out.println("Conexión Ok...");
            ResultSet rs = obj.consultarBD("select * from productos;");
            System.out.println("rs = " + rs);
            // en la función consultarBD, faltó establecer el ResultSet (rs)
            System.out.println("| id | nombre | cantidad | categoria | precio |");
            while (rs.next()){
                System.out.println("| " + rs.getInt("id") + " | " +
                        rs.getString("nombre") +  " | " +
                        rs.getInt("cantidad") + " | " +
                        rs.getString("categoria") + " | " +
                        rs.getDouble("precio"));
            }

        } catch (Exception e){
            System.out.println("Se produjo un error....");
            System.out.println(e);
        } finally {
            obj.cerrarConexion();
        }

        Vista_Presentacion ventana = new Vista_Presentacion();
        ventana.setSize(800, 600);
        ventana.setVisible(true);
        ventana.setTitle("Ejemplo Similar Reto 5 - Java MisiónTIC UIS 2022");
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
