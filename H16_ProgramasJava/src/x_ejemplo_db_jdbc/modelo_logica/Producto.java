package x_ejemplo_db_jdbc.modelo_logica;

import x_ejemplo_db_jdbc.controlador_persistencia.ConexionBD;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Producto {
    // Atributos
    private int id, cantidad;
    private String nombre, categoria;
    private double precio;

    // Constructor
    public Producto() {}

    public Producto(String nombre, int cantidad,  String categoria, double precio) {
        this.cantidad = cantidad;
        this.nombre = nombre;
        this.categoria = categoria;
        this.precio = precio;
    }

    // Métodos - Funciones

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    // CRUD => Create - Read - Update - Delete
    // Create => INSERT => crear, registrar, insertar, guardar...
    public boolean guardarProducto(){
        ConexionBD conexionBD = new ConexionBD();
        // Sentencia SQL
        // INSERT INTO producotos(nombre, cantidad, categoria, precio) VALUES('Lomo Res', 6, 'Carnes', 16000.0);
        String sql = "INSERT INTO productos(nombre, cantidad, categoria, precio) " +
                "VALUES('" + nombre + "', " + cantidad + ", '" + categoria + "', " + precio + ");";
        if (conexionBD.setAutoCommitBD(false)){
            if (conexionBD.insertetarBD(sql)){
                conexionBD.commitBD();
                conexionBD.cerrarConexion();
                return true;
            }
            else {
                conexionBD.rollbackBD();
                conexionBD.cerrarConexion();
                return false;
            }
        }
        else {
            conexionBD.cerrarConexion();
            return false;
        }
    }

    // Read => SELECT => leer, consultar, revisar datos...
    public List<Producto> listarProductos(){
        List<Producto> listaProductos = new ArrayList<>();
        ConexionBD conexionBD = new ConexionBD();
        // Sentencia SQL
        // SELECT * FROM productos;
        String sql = "SELECT * FROM productos;";
        try {
            ResultSet rs = conexionBD.consultarBD(sql);
            Producto producto;
            while (rs.next()){
                producto = new Producto();
                producto.setId(rs.getInt("id"));
                producto.setNombre(rs.getString("nombre"));
                producto.setCantidad(rs.getInt("cantidad"));
                producto.setCategoria(rs.getString("categoria"));
                producto.setPrecio(rs.getDouble("precio"));
                listaProductos.add(producto);
            }
        } catch (SQLException e){
            System.out.println("Error en lista de productos: " + e.getMessage());
        } finally {
            conexionBD.cerrarConexion();
        }
        return listaProductos;
    }

    // Upadate => UPDATE => actualizar, modificar los datos
    public boolean actualizarProducto(){
        ConexionBD conexionBD = new ConexionBD();
        // Sentencia SQL
        // UPDATE productos SET nombre = 'Lomo de Res', cantidad = 16, categoria = 'Carnes', precio = 26000.0 WHERE id = 8;
        String sql = "UPDATE productos SET nombre = '" + nombre +
                "', cantidad = " + cantidad +
                ", categoria = '" + categoria +
                "', precio = " + precio +
                " WHERE id = " + id + ";";
        if (conexionBD.setAutoCommitBD(false)){
            if (conexionBD.actualizarBD(sql)){
                conexionBD.commitBD();
                conexionBD.cerrarConexion();
                return true;
            }
            else {
                conexionBD.rollbackBD();
                conexionBD.cerrarConexion();
                return false;
            }
        }
        else {
            conexionBD.cerrarConexion();
            return false;
        }
    }

    // Delete => DELETE => eliminar, borrar, remover datos
    public boolean eliminarProducto(){
        ConexionBD conexionBD = new ConexionBD();
        // Sentencia SQL
        // DELETE FROM productos WHERE id = 8;
        String sql = "DELETE FROM productos WHERE id = " + id + ";";
        if (conexionBD.setAutoCommitBD(false)){
            if (conexionBD.borrarBD(sql)){
                conexionBD.commitBD();
                conexionBD.cerrarConexion();
                return true;
            }
            else {
                conexionBD.rollbackBD();
                conexionBD.cerrarConexion();
                return false;
            }
        }
        else {
            conexionBD.cerrarConexion();
            return false;
        }
    }
}
