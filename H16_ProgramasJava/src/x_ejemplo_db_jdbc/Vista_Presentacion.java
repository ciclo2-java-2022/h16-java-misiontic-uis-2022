package x_ejemplo_db_jdbc;

import x_ejemplo_db_jdbc.modelo_logica.Producto;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class Vista_Presentacion extends JFrame {
    // Atributos
    private JTabbedPane tabbedPane1;
    private JPanel panel;
    private JPanel panel1;
    private JPanel panel2;
    private JTextField idReg_textField1;
    private JTextField nombReg_textField2;
    private JTextField cantReg_textField3;
    private JComboBox catReg_comboBox1;
    private JTextField precReg_textField4;
    private JButton guardarButton;
    private JTable table1;
    private JTextField idCAE_textField5;
    private JTextField nombCAE_textField6;
    private JTextField cantCAE_textField7;
    private JComboBox catCAE_comboBox2;
    private JTextField precCAE_textField8;
    private JButton consultarButton;
    private JButton actualizarButton;
    private JButton eliminarButton;

    // Constructor
    public Vista_Presentacion(){
        setContentPane(panel);
        pack();
        guardarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Código Botón Guardar
                String nombre = nombReg_textField2.getText();
                int cantidad = Integer.parseInt(cantReg_textField3.getText());
                String categoria = catReg_comboBox1.getSelectedItem().toString();
                double precio = Double.parseDouble(precReg_textField4.getText());

                Producto producto = new Producto(nombre, cantidad, categoria, precio);
                if (producto.guardarProducto()){
                    System.out.println("Producto guardado...!");
                }
                else {
                    System.out.println("Error al guardar producto...");
                }
            }
        });
        consultarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Código Botón Consultar Productos...
                Object[][] filaDatos = new Object[1][5];
                Object[] columNomb = {"Id", "Nombre", "Cantidad", "Categoría", "Precio"};
                DefaultTableModel model = new DefaultTableModel();
                model.setColumnIdentifiers(columNomb);
                table1.setModel(model);

                Producto producto = new Producto();
                List<Producto> lista = producto.listarProductos();
                for (Producto i : lista) {
                    filaDatos[0][0] = i.getId();
                    filaDatos[0][1] = i.getNombre();
                    filaDatos[0][2] = i.getCantidad();
                    filaDatos[0][3] = i.getCategoria();
                    filaDatos[0][4] = i.getPrecio();
                    model.addRow(filaDatos[0]);
                }
            }
        });
        actualizarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Código Botón Actualizar
                int id = Integer.parseInt(idCAE_textField5.getText());
                String nombre = nombCAE_textField6.getText();
                int cantidad = Integer.parseInt(cantCAE_textField7.getText());
                String categoria = catCAE_comboBox2.getSelectedItem().toString();
                double precio = Double.parseDouble(precCAE_textField8.getText());

                Producto producto = new Producto(nombre, cantidad, categoria, precio);
                producto.setId(id);
                if (producto.actualizarProducto()){
                    System.out.println("Producto Actualizado...!");
                }
                else {
                    System.out.println("Error al actualizar producto...");
                }
            }
        });
        eliminarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Código Botón Eliminar
                int id = Integer.parseInt(idCAE_textField5.getText());
                String nombre = nombCAE_textField6.getText();
                int cantidad = Integer.parseInt(cantCAE_textField7.getText());
                String categoria = catCAE_comboBox2.getSelectedItem().toString();
                double precio = Double.parseDouble(precCAE_textField8.getText());

                Producto producto = new Producto(nombre, cantidad, categoria, precio);
                producto.setId(id);
                if (producto.eliminarProducto()){
                    System.out.println("Producto Eliminado...!");
                }
                else {
                    System.out.println("Error al eliminar producto...");
                }
            }
        });
    }

    // Métodos - Funciones
}
