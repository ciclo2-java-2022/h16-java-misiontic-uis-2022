import java.util.Scanner;

public class D1_convPesosDolares {

    public static void main(String[] args) {
        // Programa para convertir de pesos colombianos a dólares.
        // Declaración de Variables
        Scanner input = new Scanner(System.in);
        float trm, cantPesosTengo, cantDolares;  // trm => tasa representativa monetaria del dólar

        // Entrada(s)
        trm = 3912.15f;
        // cantPesosTengo = 10000f;
        System.out.println("Digite la cantidad de pesos que tiene:");
        cantPesosTengo = input.nextFloat();

        // Transformación
        cantDolares = cantPesosTengo * 1 / trm;

        // Salida(s)
        System.out.println("Ud, tiene USD$" + cantDolares + " dólares.");
    }
}
